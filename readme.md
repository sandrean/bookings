# A Bookings and Reservations Web Application

A web application project utilizing Go Lang. The premise of this project is to create a basic bookings and reservation site. This site was created in while following the udemy course, "Building Modern Web Application with Go by Trevor Sawler (https://www.udemy.com/course/building-modern-web-applications-with-go/)

### Dependencies:
- github.com/alexedwards/scs/v2 v2.5.0
- github.com/go-chi/chi/v5 v5.0.7
- github.com/justinas/nosurf v1.1.1