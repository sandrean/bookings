package models

// Template holds data sent from handlers to templates
type TemplateData struct {
	StringMap map[string]string
	IntMap    map[string]int
	FloatMap  map[string]float32
	Data      map[string]interface{} // anytype of data can be stored in interface{} (must have curly bracket)
	CSRFToken string                 // cross site request forgery token for security on forms
	Flash     string
	Warning   string
	Error     string
}
